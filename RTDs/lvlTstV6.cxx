#include <iostream>
#include <modbus.h>
#include <stdlib.h>
#include <time.h>
#include <errno.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
  if(argc <= 1) {
    std::cout << "Closing program...." << std::endl;
    std::cout << "Please enter the data fetching interval .." 
	      << std::endl;
    return 0;
  }

  // Set up the data taking rate
  int delay_seconds = atoi(argv[1]);
  std::cout << "Data fetching will be delayed by "
	    << delay_seconds << " seconds" 
	    << std::endl;

  // Set up a counter
  int counter = atoi(argv[2]);

  // Set up a timer
  time_t seconds;
  seconds = time(NULL);
  //  std::cout << "utc time, seconds = " << seconds << std::endl;

  uint16_t tab_reg[4]; //This is for the two channel board
  uint16_t tab_reg2[6]; //This is for the first six channel board (newer one)
  uint16_t tab_reg3[6]; //This is for the second six channel board (older one - contains the black box and heat sink)
  
  //modbus_t *mb = modbus_new_tcp("192.168.1.2", 502);
  modbus_t *mb2 = modbus_new_tcp("192.168.1.99", 502);
  modbus_t *mb3 = modbus_new_tcp("192.168.1.100", 502);
  //modbus_connect(mb);
  modbus_connect(mb2);
  modbus_connect(mb3);
  //modbus_set_debug(mb, FALSE);
  modbus_set_debug(mb2, FALSE);
  modbus_set_debug(mb3, FALSE);

  //  modbus_write_register(mb, 0, 66);
  
  // Function for reading values from the register
  float lvl0_adc(0);
  for(int i=0; i<counter; ++i) {
    if(modbus_read_input_registers(mb2,0, 6, tab_reg2)==-1 || modbus_read_input_registers(mb3,0, 6, tab_reg3)==-1 ){
      //std::cout << modbus_strerror(errno) << " - restarting servers" << std::endl;
      //modbus_close(mb);
      //modbus_free(mb);
      modbus_close(mb2);
      //modbus_free(mb2);
      modbus_close(mb3);
      //modbus_connect(mb);
      modbus_connect(mb2);
      modbus_connect(mb3);
      //modbus_set_debug(mb,  FALSE);
      modbus_set_debug(mb2, FALSE);
      modbus_set_debug(mb3, FALSE);
    }
    std::cout << time(NULL) << "\t";
    for (int a=0; a<6; ++a) std::cout << 0.00195313*float(tab_reg2[a]) << "\t";
    for (int k=0; k<6; ++k) std::cout << 0.00195313*float(tab_reg3[k]) << "\t";
    std::cout<<std::endl;
    sleep(delay_seconds);
  }

//  modbus_close(mb);
//  modbus_free(mb);
  modbus_close(mb2);
  modbus_free(mb2);
  modbus_close(mb3);
  modbus_free(mb3);

  
  return 0;
}
