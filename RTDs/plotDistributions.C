#include "TApplication.h"
#include "TAxis.h"
#include "TGraph.h"
#include "TLegend.h"
#include "TMath.h"
#include "TRandom3.h"
#include "TF1.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TH1.h"
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

// Compile with:
// g++ plotDistributions.C -o makePlotsofCoefficients `root-config --cflags --glibs`

bool plotResiduals = true;

void setRange(TH1D* hist){
  double xMinBefore = hist->GetXaxis()->GetXmin();
  double xMaxBefore = hist->GetXaxis()->GetXmax();
  double rangeBefore = xMaxBefore-xMinBefore;
  double xMaxSet = hist->GetMean()+3*hist->GetRMS();
  double xMinSet = hist->GetMean()-3*hist->GetRMS();
  double rangeSet = xMaxSet-xMinSet;
  hist->SetAxisRange(xMinSet,xMaxSet,"X");
  int difference = rangeBefore/rangeSet;
  std::cout<<difference<<std::endl;
  hist->Rebin(difference*10);
  //hist->SetAxisRange(6,9,"X");
}


int main(int argc, char *argv[]) {

  TH1D* hist0a = new TH1D("p0a","p0a",10000,209,211);
  TH1D* hist1a = new TH1D("p1a","p1a",10000,2.35,2.52);
  TH1D* hist2a = new TH1D("p2a","p2a",10000,-0.01,0.01);
  TH1D* hist3a = new TH1D("p3a","p3a",10000,-0.0002,0.0002);
  
  TF1 **function = new TF1 *[200];
  float rangeLower = 55;
  float rangeHigher = 110;
  if (plotResiduals){
	for (int i = 0; i < 200; ++i) { //populate the arrays of 
    function[i] = new TF1("f1","[0]+[1]*(x-75)+[2]*(x-75)*(x-75)+[3]*(x-75)*(x-75)*(x-75)",rangeLower,rangeHigher);
    }
  }
	
  std::string filename = std::string(argv[1]);
  std::cout <<"Using file "<<filename<<std::endl;
  int a,b;
  double c,d,e,f;
  std::ifstream file;
  int numberOfLines(9);
  int counter(0);
  file.open(filename);
  while (!file.eof()){
    file >> a >> b >> c >> d >> e >> f;
    //std::cout<<a<<"\t"<<b<<"\t"<<c<<"\t"<<d<<"\t"<<e<<"\t"<<f<<std::endl;
    if(c!=0){
	
      if(a==1701&&b==5) continue;//VC3
      if(a==2501&&b==3) continue;//VD3
      if(a==2101&&b==10) continue;//XE1
      if(a==3007&&b==9) continue;//VR3
      hist0a->Fill(c);
      hist1a->Fill(d);
      hist2a->Fill(e);
      hist3a->Fill(f);
	  if(plotResiduals){
		std::string nameStr = "Thermometer" + std::to_string(a) + "," + std::to_string(b);
		char name[20];
		strcpy(name, nameStr.c_str());
		function[counter]->SetName(name);
		function[counter]->SetParameter(0,c);
		function[counter]->SetParameter(1,d);
		function[counter]->SetParameter(2,e);
		function[counter]->SetParameter(3,f);
		}
		counter++;
      }
  }
  
      TH1D* hist0 = new TH1D("p0","p0",25,hist0a->GetMean()-3*hist0a->GetRMS(),hist0a->GetMean()+3*hist0a->GetRMS());
      TH1D* hist1 = new TH1D("p1","p1",25,hist1a->GetMean()-3*hist1a->GetRMS(),hist1a->GetMean()+3*hist1a->GetRMS());
      TH1D* hist2 = new TH1D("p2","p2",25,hist2a->GetMean()-3*hist2a->GetRMS(),hist2a->GetMean()+3*hist2a->GetRMS());
      TH1D* hist3 = new TH1D("p3","p3",25,hist3a->GetMean()-3*hist3a->GetRMS(),hist3a->GetMean()+3*hist3a->GetRMS());
  
  std::ifstream file2; //this is technically the first file in disguise! 
  int numberOfLines2(9);
  int counter2(0);
  file2.open(filename);
 
  while (!file2.eof()){
    file2 >> a >> b >> c >> d >> e >> f;
    if(c!=0){
      if(a==1701&&b==5) continue;//VC3
      if(a==2501&&b==3) continue;//VD3
      if(a==2101&&b==10) continue;//XE1
      if(a==3007&&b==9) continue;//VR3
      std::cout<<a<<"\t"<<b<<"\t"<<c<<"\t"<<d<<"\t"<<e<<"\t"<<f<<std::endl;
      hist0->Fill(c);
      hist1->Fill(d);
      hist2->Fill(e);
      hist3->Fill(f);
      }
      
  } 
  TApplication theApp("App", &argc, argv);
  TCanvas *c1 = new TCanvas();
  c1->Divide(2,2);
  c1->cd(1);
  //setRange(hist0);
  hist0->Draw();
  c1->cd(2);
  //setRange(hist1);
  hist1->Draw();
  c1->cd(3);
  //setRange(hist2);
  hist2->Draw();
  c1->cd(4);
  //setRange(hist3);
  hist3->Draw();
 
//Section to plot the residuals for the mean of the distributions. 
if(plotResiduals){
	TCanvas* c2 = new TCanvas();
	TF1* averageFunction = new TF1("averageFunction","[0]+[1]*(x-75)+[2]*(x-75)*(x-75)+[3]*(x-75)*(x-75)*(x-75)",rangeLower,rangeHigher);
	//To do this with the average values: old way:
	//averageFunction->SetParameter(0,hist0->GetMean());
	//averageFunction->SetParameter(1,hist1->GetMean());
	//averageFunction->SetParameter(2,hist2->GetMean());
	//averageFunction->SetParameter(3,hist3->GetMean());
	TGraph* GRaverageFitting = new TGraph();
	int pointNumber = 200;
	float step = (rangeHigher-rangeLower)/(float)pointNumber;
	std::cout<<"Now calculating the average function with"<< pointNumber << "interpolating points"<<std::endl;
	int numberValid(0);
	float pointAverage(0);
	for(int d = 0;d<pointNumber;d++){
		for(int e = 0; e<200;e++){
			if(d==0) std::cout<<function[e]->GetParameter(0)<<std::endl;
			if(function[e]->GetParameter(0)!=0){
				pointAverage+=function[e]->Eval(rangeLower+(d*step));
				numberValid++;
			}
		}
		pointAverage=pointAverage/(float)numberValid;	
		GRaverageFitting->SetPoint(d,rangeLower+(d*step),pointAverage);
		pointAverage = 0;
		numberValid = 0;
	}
	GRaverageFitting->Fit("averageFunction");
	GRaverageFitting->Draw();
	
	 ///Comparison
	for( int n =0; n<counter; n++){
	int nPoints = 200;
	TGraph* graphTemp = new TGraph();
	for (int x =0; x<nPoints; ++x){
		double xMax = averageFunction->GetXmax();
		if (function[n]->GetXmax()<averageFunction->GetXmax()) xMax = function[n]->GetXmax();
		double xMin = averageFunction->GetXmin();
		if (function[n]->GetXmin()>averageFunction->GetXmin()) xMax = function[n]->GetXmin();
		double interval = (xMax-xMin)/(double)nPoints;
		//std::cout<<xMax<<std::endl;
		//std::cout<<xMin<<std::endl;
		//std::cout<<interval<<std::endl;
		double average = averageFunction->Eval(xMin+(x*interval));
		double functionValueTemp = function[n]->Eval(xMin+(x*interval));
		graphTemp->SetPoint(x,xMin+(x*interval),functionValueTemp-average);
		} 
	std::cout<<"Thermometer "<<function[n]->GetName()<<std::endl; 
	graphTemp->Fit(function[n]);
	std::cout<<"-----------------------------------------------------------"<<std::endl;
	delete graphTemp;
	
	for(int y=0; y<counter; y++){
		//function[y]=new TF1(function[y]->GetName(),"[&](double *x){ return function[y](x) - averageFunction(x); }",rangeLower,rangeHigher,1);
		function[y]->SetLineColor(y+1);
		if (y==0) {
			function[y]->Draw(""); 
			function[y]->GetYaxis()->SetRangeUser(-1.1,1.1); 
			function[y]->GetYaxis()->SetTitle("Residual (function - average)[K]");
			function[y]->GetXaxis()->SetTitle("Resistance of thermometer [#Omega]");
			function[y]->SetTitle("");
			}
		else function[y]->Draw("SAME"); 
	}
	}
	std::cout<<"Parameters for average fit"<<std::endl;
	averageFunction->Print();
	std::cout<<"-----------------------------------------------"<<std::endl;
	std::cout<<"Average parameters"<<std::endl;
	std::cout<<hist0->GetMean()<<std::endl;
	std::cout<<hist1->GetMean()<<std::endl;
	std::cout<<hist2->GetMean()<<std::endl;
	std::cout<<hist3->GetMean()<<std::endl;
	std::cout<<"-----------------------------------------------"<<std::endl;
}
 theApp.Run();
}
