#include "TApplication.h"
#include "TAxis.h"
#include "TGraph.h"
#include "TLegend.h"
#include "TMath.h"
#include "TF1.h"
#include "TFile.h"
#include "TRandom3.h"
#include "TCanvas.h"
#include "TROOT.h"
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <stdio.h>

// Compile with:
// g++ resistorCalibrationwithPIDv2.cc -o PIDCalibrationRuns `root-config --cflags --glibs`
//or
// g++ resistorCalibrationwithPIDv2.cc -o PIDCalibrationRunsBatch `root-config --cflags --glibs`

// Usage:
// ./resistorCalibration <dataFile> <plotType> <cutsFile> <filename>
// (plotType ->  0 = reference resistor,  time in hours and minutes; 1 = RR,
// time in seconds;
//               2 = all RTDs against RR temperature 3= all RTDs against Temp reduced (calibration curve) 4=All resistors as a
//               function of time only cut regions. 5= DeltaR distributions to reference sensor. 6= Long term hysteresis study. )
// (startSeconds & endSeconds are start and end time to do the fitting for
// plotType = 1, and to
// plot graph points for plotType = 2)

using namespace std;

bool BatchJob = false;

int main(int argc, char *argv[]) {
	
	if (BatchJob)gROOT->SetBatch(kTRUE);
	
	//Read in the arguments and set global variables(see above)
  bool useFileList = false;
  if (string(argv[1]).find("list") != string::npos)
    useFileList = true;
  int plotType = 0;
  if (argc > 2)
    plotType = atoi(argv[2]);
  std::string filenameCuts = string(argv[3]);
  std::string outputFilename = string(argv[4]);
  char outputFilenameC[1024];
	strcpy(outputFilenameC, outputFilename.c_str());
  long int time;
  double ref, a, b, c, d, e, f, p, q, r, s, u, v;
  double rtds[12]={0};
  double rtdsFirst[12]={0};
  double rtdsEnd[12]={0};
  TRandom3 random;
  TGraph *gr = new TGraph(); //for the single plots
  TGraph **grs = new TGraph *[12]; //for the RTD values (one for each channel)
  TGraph **grsReduced = new TGraph *[12]; //for the RTD values when we only have the 6 points for fitting the cubic. 
  TGraph **grsTime = new TGraph *[12]; //for the RTD values as a functin of time.
  TGraph **grsDeltaR = new TGraph *[12]; //for the delta R distributions as a function of time. 
 
  int n3(0); //counters
  int n = 0;
  for (int i = 0; i < 12; ++i) { //populate the arrays of TGraphs with the TGraphs and then give them names.
    grs[i] = new TGraph();
    string nameStr = "Channel" + std::to_string(i);
    char name[10];
    strcpy(name, nameStr.c_str());
    grs[i]->SetName(name);
    grs[i]->GetXaxis()->SetRangeUser(0, 120);
    grsTime[i] = new TGraph();
    grsTime[i]->SetName(name);
    grsDeltaR[i] = new TGraph();
    grsDeltaR[i]->SetName(name);
    grsReduced[i] = new TGraph();
    grsReduced[i] ->SetName(name);
  }
  

  // Calibration parameters from 100K to 325K (high) and for less than 100K (low) for the reference sensor. 
  double AiHigh[] = {208.627669, 120.086675, 1.484563, -0.131668, 0.028402};
  double ZlHigh = 25.77140014399;
  double ZuHigh = 122.10187786887;
  double AiLow[] = {58.086947, 46.882945, 10.555113, 3.847734, 0.547042,
		    0.241445, -0.030391, 0.020996, -0.011393, 0.005496, -0.002593, 0.001663};
  double ZlLow = 0.28058205738;
  double ZuLow = 1.58477856815;

  
  //if we have a list of files this is where it is read in. 
  ifstream infile;
  string line;
  bool endLoop = false;
  vector<string> fileNames;
  int noOfFiles = 1;
  if (!useFileList)
    fileNames.push_back(string(argv[1]));
  else {
    ifstream infile2(argv[1]);
    --noOfFiles;
    while (getline(infile2, line)) {
      fileNames.push_back(line);
      ++noOfFiles;
    }
    infile2.close();
  }
  int first = 0; //time at the start of the run 
  TApplication theApp("App", &argc, argv);
  
  double grad = 0;
  double time2 = 0;
  int n2 = 0;

  // Set up the data cuts:
  int nCuts = 0;
  std::vector<unsigned long int> cutStart;
  std::vector<unsigned long int> cutEnd;
  long int start;
  long int end;
  ifstream infileCuts;
  infileCuts.open(filenameCuts);
  std::string theLine;
  while (!infileCuts.eof()) {
    nCuts++;
    infileCuts >> start >> end;
    cutStart.push_back(start);
    //std::cout << start << std::endl;
    cutEnd.push_back(end);
    //std::cout << end << std::endl;
  }
  //nCuts--; //This is to account for the double counting for the last one!!
  infileCuts.close();

  
  
  int numberOfLines; //section to count number of lines in the datafiles. This is useful so that we can look at first and last lines.  
  for (int i = 0; i < noOfFiles; ++i) {
	numberOfLines=0;
	ifstream is(fileNames[i]);
    cout << "Using file " << fileNames[i] <<" to get number of lines" << endl;
	std::cout << is.is_open() << std::endl;
  
  while (!is.eof())
    {
	//std::cout<<"1"<<std::endl;
	getline(is, line);
      numberOfLines++;
    }
	std::cout<<numberOfLines<<std::endl;

  }
  
  
  //section to intiialise the variable which keeps a track of the number of points going inot each point in the final cubic fit.  
  int numberOfPointsInRange[nCuts];
  for (int l = 0; l < nCuts; l++)
      numberOfPointsInRange[l] = 0;
  int points = 6000; //This is the maximum number of points at each one of these. This will be useful for the deletion later. 
  
  //main loop over the files
  for (int i = 0; i < noOfFiles; ++i) {
    infile.open(fileNames[i]);
    cout << "Using file " << fileNames[i] << endl;
    
    getline(infile, line);
    while (infile >> time >> rtds[0] >> rtds[1] >> rtds[2] >> rtds[3] >>
           rtds[4] >> rtds[5] >> rtds[6] >> rtds[7] >> rtds[8] >> rtds[9] >>
           rtds[10] >> rtds[11]) {
      if (n == 0){
        first = time;
		std::cout<<first<<std::endl;
	  }
	 
	//Look at the first 10 data points
	if(n<10){
		 std::cout<<n<<std::endl;
	
	for(int r=0; r<12;r++){
		rtdsFirst[r]+=rtds[r]/10.0; //divide by 10 is because we want an average
		//std::cout<<"a"<<rtds[r]<<std::endl;
		//std::cout<<rtdsFirst[r]<<std::endl;
	}
	
}

	//look at the last 10 data points. Note here there is an issue with the counting. 
	if(n > numberOfLines-13){
		std::cout<<numberOfLines-n<<std::endl;
		for(int r=0; r<12;r++){
		rtdsEnd[r]+=rtds[r]/10.0;
		//std::cout<<"b"<<rtds[r]<<std::endl;
		//std::cout<<rtdsEnd[r]<<std::endl;
	}
	
	
	}
	  
		
		
      //std::cout << rtds[1] << std::endl;
      double T = 0;
      double k = 0;
      if(rtds[1] > 30.02){
		k = (2 * rtds[1] - ZlHigh - ZuHigh) /
                 (ZuHigh - ZlHigh); // this rtd[0] is pid resistor and the one in channel 1 (rtds[1]) is the reference resistor.
      	for (int i = 0; i < 5; ++i) {
        T += AiHigh[i] * cos(i * acos(k));
      	}
      }
      else{
	k = (2 * log10(rtds[1]) - ZlLow - ZuLow) /
                 (ZuLow - ZlLow);
	/*std::cout<< "k " << k <<std::endl;
	std::cout << "R " << rtds[1] << std::endl;
	std::cout << "Zl " << ZlLow <<std::endl;
	std::cout << "Zu " << ZuLow <<std::endl;*/
	for (int i = 0; i < 12; ++i){
	T += AiLow[i] * cos(i * acos(k));
        }
	//std::cout<<T<<std::endl;
      } 

      double basedTime = time - first;
      //std::cout << time <<std::endl;
      if (plotType == 0) {
		 if(time < 10E11 || (T<350 && T>0))
        gr->SetPoint(n, time, rtds[3]);
        //std::cout << T << std::endl;
      } else if (plotType == 1)
        gr->SetPoint(n, basedTime, T);
	
      else {
		for (int x = 0; x < nCuts; x++) {
          // std::cout << cutStart[a] << " " <<cutEnd[a] << " " << nCuts <<
          // std::endl;
          //std::cout << a << " " << numberOfPointsInRange[a] << std::endl;
		  
          if (cutStart[x] < basedTime && basedTime < cutEnd[x]) {
				  
			numberOfPointsInRange[x]++;
            for (int m = 0; m < 12; ++m) {
                  //std::cout<<n2<<std::endl;
                  if (T == 0)
                    std::cout << "Zero" << std::endl;
                  grs[m]->SetPoint(n2, rtds[m], T);
                  //if (m==3)std::cout << "Filling grs[] " <<n2 << "," <<numberOfPointsInRange[x] << ","<< rtds[m] << "," << T << std::endl;
                  grsTime[m]->SetPoint(n2, time, rtds[m]);
                  // std::cout << n2 << ","<<time<<","<<rtds[m]<<std::endl;
				  grsDeltaR[m]->SetPoint(n2, time, rtds[m]-rtds[1]);
                 
              }
			++n2;
            //std::cout<<"incrementing n2"<<std::endl;
            //}
          }
        }
				
      }
      time2 = time;
      ++n;
    }
    infile.close();
    for (int l = 0; l < nCuts; l++) {
      std::cout << l << " " << numberOfPointsInRange[l] << std::endl;
      if (numberOfPointsInRange[l] < points)
        std::cout << "WARNING! ONE OF THE SAMPLED REGIONS DOES NOT CONTAIN "
                  << points << " Points" << std::endl;
    }
	//This section is to select the data, by deleting random points:
		for (int x=0; x< nCuts; x++){
			int numberToCut = numberOfPointsInRange[x]-points;
			double lowerBound = 0;
			for (int f=0; f<x; ++f)
					lowerBound+=numberOfPointsInRange[f];
			//int upperBound = lowerBound + numberOfPointsInRange[x];
			for(int p = 0; p<numberToCut;++p){
				int deletePoint = lowerBound + random.Integer(numberOfPointsInRange[x]);
				//std::cout<<deletePoint<<std::endl;
				for(int m =0;m<12; ++m){
					//double xCoordinate(0);
					//double yCoordinate(0);
					//std::cout<<grs[m]->GetN()<<std::endl;
					//grs[m]->GetPoint(7,xCoordinate,yCoordinate);
					//std::cout<< xCoordinate << ", " << yCoordinate <<std::endl;
					grs[m]->RemovePoint(deletePoint);
					grsTime[m]->RemovePoint(deletePoint);
					//std::cout<<grs[m]->GetN()<<std::endl;
					//grs[m]->GetPoint(7,xCoordinate,yCoordinate);
					//std::cout<< xCoordinate << ", " << yCoordinate <<std::endl;
					
					//upperBound--;
					
				
				}
				//lowerBound--;
				numberOfPointsInRange[x]--;
			}
		}
		TGraph **linearGraphs = new TGraph *[12];
		for (int i = 0; i < 12; ++i) {
		  linearGraphs[i] = new TGraph();
		}
		TCanvas* c2 = new TCanvas("stamp","stamp");
		c2->Divide(12,nCuts);
		TCanvas* c2Range = new TCanvas("stampRange","stampRange");
		c2Range->Divide(12,nCuts);
		int canvasCounter(1);
			//This section is to perform the linear fit of a region to get one point out at the end!
			bool statement = true;
			for(int x =0; x<nCuts; ++x){
				int lowerBound = 0;
				for (int f=0; f<x; ++f)
					lowerBound+=numberOfPointsInRange[f];
				std::cout<<"THIS! "<<lowerBound<<std::endl;
				for(int m=0; m<12; ++m){
				double xCoordinateMax(0);
			    double xCoordinateMin(999);
				double xCoordinate(0);
			    double yCoordinate(0);
			    for(int z =0; z<numberOfPointsInRange[x]; ++z){
			      //std::cout<<grs[m]->GetN()<<std::endl;
			      grs[m]->GetPoint(lowerBound+z,xCoordinate,yCoordinate);
				  //if(x==5&&m==9)std::cout<<"Reading point "<<lowerBound+z<<","<<xCoordinate<<","<<yCoordinate<<std::endl;
				  if(xCoordinate>xCoordinateMax) xCoordinateMax=xCoordinate;
				  if(xCoordinate<xCoordinateMin) xCoordinateMin=xCoordinate;
			      linearGraphs[m]->SetPoint(z,xCoordinate,yCoordinate);
				  //if(x<=3&&m==3)std::cout<<"Filling linear Graphs "<<x<<";"<<m<<";"<<z<<";"<<lowerBound+z<<";"<<xCoordinate<<","<<yCoordinate<<std::endl;
			    }
			      //Fitting
			    TF1 *f1 = new TF1("f1","pol1");
				linearGraphs[m]->Fit("f1","Q");
				TGraph* plot = new TGraph(*linearGraphs[m]);
				TGraph* plotRange = new TGraph(*linearGraphs[m]);
				c2->cd(canvasCounter);
				canvasCounter++;
				plot->SetMarkerStyle(20);
				plot->SetMarkerSize(0.2);
				plot->GetYaxis()->SetTitle("Temperature (K)");
                plot->GetXaxis()->SetTitle("Resistance (#Omega)");
				plot->Draw("AP");
				c2Range->cd(canvasCounter-1);
				plotRange->SetMarkerStyle(20);
				plotRange->SetMarkerSize(0.2);
				plotRange->GetYaxis()->SetTitle("Temperature (K)");
                plotRange->GetXaxis()->SetTitle("Resistance (#Omega)");
				auto axisX = plotRange->GetXaxis();
				axisX->SetLimits(xCoordinateMin+(xCoordinateMax-xCoordinateMin)/2.0-0.02,xCoordinateMin+(xCoordinateMax-xCoordinateMin)/2.0+0.02);
				auto axisY = plotRange->GetYaxis();
				axisY->SetRangeUser(f1->Eval(xCoordinateMin+(xCoordinateMax-xCoordinateMin)/2.0)-0.04,f1->Eval(xCoordinateMin+(xCoordinateMax-xCoordinateMin)/2.0)+0.04); 
				//std::cout<<"Changed "<<f1->Eval(xCoordinateMin+(xCoordinateMax-xCoordinateMin)/2.0)-0.04<<"\t"<<f1->Eval(xCoordinateMin+(xCoordinateMax-xCoordinateMin)/2.0)+0.04<<std::endl;  
				plotRange->Draw("AP");
				if(x==1&&m==2&&plotType==3){
					std::cout<<"ENTERING IF STATEMENT"<<std::endl; 
					TCanvas* c3 = new TCanvas("individualstamp","individualstamp");
					TGraph* plot = new TGraph(*linearGraphs[m]);
					plot->Draw();
					statement=false;
				}
				for (int i = 0; i < 12; ++i) {
				delete linearGraphs[i];
				linearGraphs[i] = new TGraph();  
				}
			    //Extract information - interpolate from centre!
				grsReduced[m]->SetPoint(x,xCoordinateMin+(xCoordinateMax-xCoordinateMin)/2.0,f1->Eval(xCoordinateMin+(xCoordinateMax-xCoordinateMin)/2.0));
					//std::cout<<"Filling new graph "<<m<<","<<x<<","<<xCoordinateMax<<" , "<<xCoordinateMin<<std::endl; 
			    delete f1;
			}
			
		}
		
		//Information to save the stamp plots for the linear fitting!
		std::string stampFile = filenameCuts;
		std::string stampDate;
		int n = stampFile.find_first_of("0123456789");
		if (n != std::string::npos)
		{
		std::size_t const m = stampFile.find_first_not_of("0123456789", n);
		stampDate = stampFile.substr(n, m != std::string::npos ? m-n : m);
		}
		if (strstr(stampFile.c_str(),"Cool")!=NULL) stampDate = stampDate + "CoolstampPlots.pdf";
		if (strstr(stampFile.c_str(),"Warm")!=NULL) stampDate = stampDate + "WarmstampPlots.pdf";
		c2->SaveAs(stampDate.c_str());

			  
		for (int l = 0; l < nCuts; l++) {
      std::cout << l << " " << numberOfPointsInRange[l] << std::endl;
      if (numberOfPointsInRange[l] < points)
        std::cout << "WARNING! ONE OF THE SMAPLED REGIOS DOES NOT CONTAIN "
                  << points << " Points" << std::endl;
    }
		
	
  }
  if (plotType == 0) {
    gr->GetXaxis()->SetTimeDisplay(1);
    gr->GetXaxis()->SetTimeOffset(0, "bst");
    gr->GetXaxis()->SetTimeFormat("%H:%M");
    gr->GetXaxis()->SetTitle("Time (H:M)");
    gr->GetYaxis()->SetTitle("Temperature (K)");
	TCanvas* c4 = new TCanvas("Temperature-Time","Temperature-Time");
    gr->Draw("ap");
	std::cout << "Graph has: "<<gr->GetN() << " points" <<std::endl;
  } else if (plotType == 1) {
    // gr->Fit("pol1","","",startSeconds,endSeconds);
    gr->GetXaxis()->SetTitle("Time Since Start (s)");
    gr->GetYaxis()->SetTitle("Temperature (K)");
	TCanvas* c5 = new TCanvas("Temperature-TimeBased","Temperature-TimeBased");
    gr->Draw("ap");
  } else if (plotType == 2) {
    TLegend *leg = new TLegend(0.1, 0.4, 0.3, 0.9);
    int colours[12] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 28, 30, 49};
    stringstream name;
    int first = 0;
    bool firstFound = false;
    Double_t x1, y1, x2, y2;
    for (int m = 0; m < 12; ++m) {
      grs[m]->GetXaxis()->SetRangeUser(0, 120);
	  //std::cout << grs[m]->GetN() << std::endl;
      grs[m]->GetPoint(grs[m]->GetN() - 1, x2, y2);
      grs[m]->GetPoint(0, x1, y1);
      if (x2 - x1 < -1 || x2 - x1 > 1) {
        grs[m]->SetMarkerColor(colours[m]);
		grs[m]->SetMarkerStyle(20);
        grs[m]->SetLineColor(colours[m]);
        grs[m]->GetXaxis()->SetTitle("Temperature (K)");
        grs[m]->GetYaxis()->SetTitle("Resistance (#Omega)");
        name << "Ch " << m;
        leg->AddEntry(grs[m], name.str().c_str(), "l");
        name.str("");
        if (m == first) {
          grs[m]->Draw("ap");

          firstFound = true;
        } else
          grs[m]->Draw("psame");
      } else if (!firstFound)
        ++first;
      leg->Draw("same");
    }
	//This is to implement the fitting. 
	std::cout<<"**************************************"<<std::endl;
	std::cout<<"*********FITTING ALGORITHMS***********"<<std::endl;
	std::cout<<"**************************************"<<std::endl;
	std::cout<<"Note that channel 0 is connected to the pid controller and also channel 1 is the reference"<<std::endl;
	for (int m = 0; m< 12 ;++m){
		std::cout<<"------------------------------------------------------------------------------------"<<std::endl;
		std::cout<<" Fitting for channel "<< m << std::endl;
		grs[m]->Fit("pol5","MFR","",45,110);
		std::cout<<"------------------------------------------------------------------------------------"<<std::endl;
		
	}
  }
	else if (plotType == 3) {
    TLegend *leg = new TLegend(0.1, 0.4, 0.3, 0.9);
    int colours[12] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 28, 30, 49};
    stringstream name;
    int first = 0;
    bool firstFound = false;
    Double_t x1, y1, x2, y2;
    for (int m = 0; m < 12; ++m) {
      grsReduced[m]->GetXaxis()->SetRangeUser(0, 120);
	  //std::cout << grsReduced[m]->GetN() << std::endl;
      grsReduced[m]->GetPoint(grsReduced[m]->GetN() - 1, x2, y2);
      grsReduced[m]->GetPoint(0, x1, y1);
      if (x2 - x1 < -1 || x2 - x1 > 1) {
        grsReduced[m]->SetMarkerColor(colours[m]);
		grsReduced[m]->SetMarkerStyle(20);
        grsReduced[m]->SetLineColor(colours[m]);
        grsReduced[m]->GetYaxis()->SetTitle("Temperature (K)");
        grsReduced[m]->GetXaxis()->SetTitle("Resistance (#Omega)");
        name << "Ch " << m;
        leg->AddEntry(grsReduced[m], name.str().c_str(), "l");
        name.str("");
        if (m == first) {
		  TCanvas* c1 = new TCanvas("CalibrationCurve","CalibrationCurve");
          grsReduced[m]->Draw("ap");

          firstFound = true;
        } else
          grsReduced[m]->Draw("psame");
      } else if (!firstFound)
        ++first;
      leg->Draw("same");
    }
	//This is to implement the fitting. 
	TFile* outputFile = TFile::Open(outputFilenameC, "RECREATE");
	std::cout<<"**************************************"<<std::endl;
	std::cout<<"*********FITTING ALGORITHMS***********"<<std::endl;
	std::cout<<"**************************************"<<std::endl;
	std::cout<<"Note that channel 0 is connected to the pid controller and also channel 1 is the reference"<<std::endl;
	for (int m = 0; m< 12 ;++m){
		std::cout<<"------------------------------------------------------------------------------------"<<std::endl;
		std::cout<<" Fitting for channel "<< m << std::endl;
		double rangeLower = 45;
		double rangeHigher = 120;
		std::cout<<"Please not this fit is only in the range from "<<rangeLower<<" to "<<rangeHigher<<std::endl;
		string nameStr = "Fitting" + std::to_string(m);
		char name[10];
		strcpy(name, nameStr.c_str());
	TF1* function = new TF1(name,"[0]+[1]*(x-75)+[2]*(x-75)*(x-75)+[3]*(x-75)*(x-75)*(x-75)",rangeLower,rangeHigher);
		grsReduced[m]->Fit(name,"R"); //MFR
		function->Write();
		std::cout<<"------------------------------------------------------------------------------------"<<std::endl;
		
	}
	outputFile->Close();
	
  } else if (plotType ==4) {
	TCanvas *c6 = new TCanvas();
    TLegend *leg = new TLegend(0.1, 0.4, 0.3, 0.9);
    int colours[12] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 28, 30, 49};
    stringstream name;
    int first = 0;
    bool firstFound = false;
    Double_t x1, y1, x2, y2;
    for (int m = 0; m < 12; ++m) {
      grsTime[m]->GetPoint(grs[m]->GetN() - 1, x2, y2);
      grsTime[m]->GetPoint(0, x1, y1);
      if (x2 - x1 < -1 || x2 - x1 > 1 || true) {
        grsTime[m]->SetMarkerColor(colours[m]);
        // grsTime[m]->SetMarkerStyle(3);
        grsTime[m]->SetLineColor(colours[m]);
        grsTime[m]->GetYaxis()->SetTitle("Resistance (#Omega)");
        // grsTime[m]->GetXaxis()->SetTitle("Time since start (s)");
        grsTime[m]->GetXaxis()->SetTimeDisplay(1);
        grsTime[m]->GetXaxis()->SetTimeOffset(0, "bst");
        grsTime[m]->GetXaxis()->SetTimeFormat("%H:%M");
        grsTime[m]->GetXaxis()->SetTitle("Time (H:M)");
        name << "Ch " << m;
        leg->AddEntry(grsTime[m], name.str().c_str(), "lp");
        name.str("");
        if (m == first) {
          grsTime[m]->Draw("ap");
          firstFound = true;
        } else
          grsTime[m]->Draw("psame");
      } else if (!firstFound)
        ++first;
      leg->Draw("same");
    }
  }
	else if (plotType==5) {
	TCanvas *c7 = new TCanvas();
	TLegend *leg = new TLegend(0.1, 0.4, 0.3, 0.9);
    int colours[12] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 28, 30, 49};
    stringstream name;
    int first = 0;
    bool firstFound = false;
   	
    for (int m = 0; m < 12; ++m) {
        grsDeltaR[m]->SetMarkerColor(colours[m]);
        // grsDeltaR[m]->SetMarkerStyle(3);
        grsDeltaR[m]->SetLineColor(colours[m]);
        grsDeltaR[m]->GetYaxis()->SetTitle("#Delta Resistance R_{RTD} - R_{REF} (#Omega)");
        // grsDeltaR[m]->GetXaxis()->SetTitle("Time since start (s)");
        grsDeltaR[m]->GetXaxis()->SetTimeDisplay(1);
        grsDeltaR[m]->GetXaxis()->SetTimeOffset(0, "bst");
        grsDeltaR[m]->GetXaxis()->SetTimeFormat("%H:%M");
        grsDeltaR[m]->GetXaxis()->SetTitle("Time (H:M)");
        name << "Ch " << m;
        leg->AddEntry(grsDeltaR[m], name.str().c_str(), "lp");
        name.str("");
        if (m == first) {
          grsDeltaR[m]->Draw("ap");
          firstFound = true;
        } else
          grsDeltaR[m]->Draw("psame");
      
      leg->Draw("same");
    }	
	n3++;
	}
	else{
		std::cout<<"Running an analysis to examine the effects of long term Hysteresis"<<std::endl;
		std::cout<<"Sensors at the start and end"<<std::endl;
		std::cout<<"----------------------"<<std::endl;
		for( int beta=0; beta<12; beta++){
			std::cout<<"Sensor "<<beta<<" "<<rtdsFirst[beta]<<"\t"<<rtdsEnd[beta]<<"\t"<<rtdsFirst[beta]-rtdsFirst[1]<<"\t"<<rtdsEnd[beta]-rtdsEnd[1]<<"\t"<<abs(rtdsFirst[beta]-rtdsFirst[1])-abs(rtdsEnd[beta]-rtdsEnd[1])<<"Ohms";
			if(abs(abs(rtdsFirst[beta]-rtdsFirst[1])-abs(rtdsEnd[beta]-rtdsEnd[1]))>0.05)std::cout<<"***"<<std::endl;
			else{std::cout<<std::endl;}
		}
	}
  
  if(!BatchJob)theApp.Run();
  return 0;
}
