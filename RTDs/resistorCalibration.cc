#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include "TGraph.h"
#include "TAxis.h"
#include "TApplication.h"
#include "TMath.h"
#include "TLegend.h"


//Compile with:
//g++ resistorCalibration.cc -o resistorCalibration `root-config --cflags --glibs`

//Usage:
// ./resistorCalibration <dataFile> <plotType> <startSeconds> <endSeconds>
// (plotType ->  0 = reference resistor,  time in hours and minutes; 1 = RR, time in seconds + fit;
//               2 = all RTDs against RR temperature )
// (startSeconds & endSeconds are start and end time to do the fitting for plotType = 1, and to
// plot graph points for plotType = 2)


using namespace std;

int main(int argc, char *argv[]){
  bool useFileList = false;
  if(string(argv[1]).find("list") != string::npos) useFileList = true;
  int plotType = 0;
  int startSeconds = 0;
  int endSeconds = 0;
  if(argc>2) plotType = atoi(argv[2]);
  if(argc>3) startSeconds = atoi(argv[3]);
  if(argc>4) endSeconds = atoi(argv[4]);
  int time;
  double ref, a, b, c, d, e, f, p, q, r, s, u, v;
  double rtds[12];
  string line;
  TGraph* gr = new TGraph();
  TGraph** grs = new TGraph*[12];
  TGraph** grsTime = new TGraph*[12];
  for(int i = 0; i<12; ++i){
    grs[i] = new TGraph();
    grsTime[i] = new TGraph();
  }
  int n = 0;

  //Calibration parameters from 100K to 325K
  double Ai[] = {208.616649,120.081022,1.496497,-0.126197,0.026807,-0.004725,-0.0092523};
  double Zl = 25.78219108867;
  double Zu = 122.13398077804;
  ifstream infile;
  bool endLoop = false;
  vector<string> fileNames;
  int noOfFiles = 1;
  if(!useFileList) fileNames.push_back(string(argv[1]));
  else{
    ifstream infile2(argv[1]);
    --noOfFiles;
    while(getline(infile2,line)){
      fileNames.push_back(line);
      ++noOfFiles;
    }
    infile2.close();
  }
  int first = 0;
  TApplication theApp("App",&argc, argv);
  double grad = 0;
  double time2 = 0;
  int n2 = 0;
  for(int i = 0; i<noOfFiles; ++i){
    infile.open(fileNames[i]);
    cout << "Using file " << fileNames[i] << endl;
    getline(infile,line);
    while(infile >> time >> ref >> rtds[0] >> rtds[1] >> rtds[2] >> rtds[3] >> rtds[4]
	         >> rtds[5] >> rtds[6] >> rtds[7] >> rtds[8] >> rtds[9] >> rtds[10] >> rtds[11]){
      if(n==0) first = time;
      double k = (2*ref-Zl-Zu)/(Zu-Zl);
      double T = 0;
      for(int i = 0; i<6; ++i){
	T += Ai[i]*cos(i*acos(k));
      }
      double basedTime = time-first;
      if(plotType==0)
	gr->SetPoint(n,time,T);
      else if(plotType==1)
	gr->SetPoint(n,basedTime,T);
      else{	
	if(basedTime>startSeconds
	   && (endSeconds==0 || (endSeconds!=0 && basedTime<endSeconds))){
	  for(int m = 0; m<12; ++m){
	    grs[m]->SetPoint(n2,rtds[m],T);
	    grsTime[m]->SetPoint(n2,time,rtds[m]);
	  }
	  ++n2;
	}
      }
      time2 = time;
      ++n;
    }
    infile.close();
  }
  if(plotType==0){
    gr->GetXaxis()->SetTimeDisplay(1);
    gr->GetXaxis()->SetTimeOffset(0,"bst");             
    gr->GetXaxis()->SetTimeFormat("%H:%M");
    gr->GetXaxis()->SetTitle("Time (H:M)");
    gr->GetYaxis()->SetTitle("Temperature (K)");
    gr->Draw("ap");
  }
  else if(plotType==1){
   gr->Fit("pol1","","",startSeconds,endSeconds);
   gr->GetXaxis()->SetTitle("Time Since Start (s)");
   gr->GetYaxis()->SetTitle("Temperature (K)");
   gr->Draw("ap");
  }
  else if(plotType==2){
    TLegend* leg = new TLegend(0.1,0.4,0.3,0.9);
    int colours[12] = {1,2,3,4,5,6,7,8,9,28,30,49};
    stringstream name;
    int first = 0;
    bool firstFound = false;
    Double_t x1, y1, x2, y2;
    for(int m = 0; m<12; ++m){
      grs[m]->GetPoint(grs[m]->GetN()-1,x2,y2);
      grs[m]->GetPoint(0,x1,y1);
      if(x2-x1<-1||x2-x1>1){
	grs[m]->SetMarkerColor(colours[m]);
	grs[m]->SetLineColor(colours[m]);
	grs[m]->GetYaxis()->SetTitle("Temperature (K)");
	grs[m]->GetXaxis()->SetTitle("Resistance (#Omega)");
	name << "Ch " << m+1;
	leg->AddEntry(grs[m],name.str().c_str(),"l");
	name.str("");
	if(m==first){
	  grs[m]->Draw("ap");
	  firstFound = true;
	}
	else grs[m]->Draw("psame");
      }
      else if(!firstFound) ++first;
      leg->Draw("same");
    }
  }
  else{
    TLegend* leg = new TLegend(0.1,0.4,0.3,0.9);
    int colours[12] = {1,2,3,4,5,6,7,8,9,28,30,49};
    stringstream name;
    int first = 0;
    bool firstFound = false;
    Double_t x1, y1, x2, y2;
    for(int m = 0; m<12; ++m){
      grsTime[m]->GetPoint(grs[m]->GetN()-1,x2,y2);
      grsTime[m]->GetPoint(0,x1,y1);
      if(x2-x1<-1||x2-x1>1||true){
	grsTime[m]->SetMarkerColor(colours[m]);
	grsTime[m]->SetLineColor(colours[m]);
	grsTime[m]->GetYaxis()->SetTitle("Resistance (#Omega)");
	//grsTime[m]->GetXaxis()->SetTitle("Time since start (s)");
	grsTime[m]->GetXaxis()->SetTimeDisplay(1);
    	grsTime[m]->GetXaxis()->SetTimeOffset(0,"bst");
    	grsTime[m]->GetXaxis()->SetTimeFormat("%H:%M");
    	grsTime[m]->GetXaxis()->SetTitle("Time (H:M)");
    	//grsTime[m]->GetYaxis()->SetTitle("Temperature (K)");
	name << "Ch " << m+1;
	leg->AddEntry(grsTime[m],name.str().c_str(),"lp");
	name.str("");
	if(m==first){
	  grsTime[m]->Draw("apl");
	  firstFound = true;
	}
	else grsTime[m]->Draw("same");
      }
      else if(!firstFound) ++first;
      leg->Draw("same");
    }
  }
  theApp.Run();
  return 0;
}
