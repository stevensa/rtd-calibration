//This is a file which is designed to take as input the file containing the plots for the Xe Kr Ratio. This macro just makes them look nice. 
//To compile this file after loading ROOT in: g++ combineWarmandCool.C -o combineWarmandCool `root-config --cflags --glibs`
// Standard C++ headers
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cmath>
#include <stdio.h>
#include <time.h>

// ROOT Headers
#include "TROOT.h"
#include "TSystem.h"
#include "TStopwatch.h"
#include "TFile.h"
#include "TString.h"
#include "TCanvas.h"
#include "TTree.h"
#include "TChain.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TH3F.h"
#include "TProfile.h"
#include "TMath.h"
#include "TTimeStamp.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TF1.h"



int main(int argc, char *argv[]){
	std::string date = std::string(argv[1]);
	//std::string date = "2201";
	std::string outfileName = date + "values.txt"; 
	std::ofstream outfile;
	outfile.open(outfileName);
	
	//////////////////////////////////////////////////////////
	//Warm Up
	//////////////////////////////////////////////////////////
	TString name = "warmUp" + date + ".root";
	TFile *infile = new TFile(name, "read");
	
	
	std::cout<<"Files opened successfully" <<std::endl;
	
	
	TF1 **warmUp = new TF1 *[12];
	
	for(int i=0;i<12;++i){
		std::string nameStr = "Fitting" + std::to_string(i);
		char name[10];
		strcpy(name, nameStr.c_str());
		warmUp[i] = (TF1*)infile->Get(name);
		if(i == 9||true){
		std::cout<<"------------------------------------------------------------------------------------"<<std::endl;
		std::cout<<"Channel "<<i<<" Cool DOWN"<<std::endl;
		warmUp[i]->Print();
		std::cout<<"------------------------------------------------------------------------------------"<<std::endl;
		}
	}
	
	//////////////////////////////////////////////////////////
	//Cool Down
	//////////////////////////////////////////////////////////
	TString name2 = "coolDn" + date + ".root";
	TFile *infile2 = new TFile(name2, "read");
	
	
	std::cout<<"Files opened successfully" <<std::endl;
	
	
	TF1 **coolDn = new TF1 *[12];
	
	for(int i=0;i<12;++i){
		std::string nameStr = "Fitting" + std::to_string(i);
		char name[10];
		strcpy(name, nameStr.c_str());
		coolDn[i] = (TF1*)infile2->Get(name);
		if(i == 9||true){
		std::cout<<"------------------------------------------------------------------------------------"<<std::endl;
		std::cout<<"Channel "<<i<<" Warm UP"<<std::endl;
		coolDn[i]->Print();
		std::cout<<"------------------------------------------------------------------------------------"<<std::endl;
		}
	}
	
	std::cout<<"------------------------------------------------------------------------------------"<<std::endl;
	std::cout<<"Interpolating these functions"<<std::endl;
	
	TGraph **grsReduced = new TGraph *[12];
	
	
	for(int m=0;m<12;++m){
		grsReduced[m] = new TGraph();
		std::string nameStr = "Channel" + std::to_string(m);
		char name[10];
		strcpy(name, nameStr.c_str());
		grsReduced[m]->SetName(name);
		
		int nPoints = 20;
		for (int x =0; x<nPoints; ++x){
		double xMax = coolDn[m]->GetXmax();
		double xMin = coolDn[m]->GetXmin();
		double interval = (xMax-xMin)/(double)nPoints;
		//std::cout<<xMax<<std::endl;
		//std::cout<<xMin<<std::endl;
		//std::cout<<interval<<std::endl;
		double warmY = warmUp[m]->Eval(xMin+(x*interval));
		double coldY = coolDn[m]->Eval(xMin+(x*interval));
		grsReduced[m]->SetPoint(x,xMin+(x*interval),(warmY+coldY)/2.0);
	std::cout<<"Difference between the two is for sensor "<<m<<" "<<warmY-coldY<<"K"<<std::endl;
		//std::cout<<x<<" "<<xMin+(x*interval)<<" "<<(warmY+coldY)/2.0<<std::endl;
		
		//std::cout<<"THIS PART"<<grsReduced[m]->GetN()<<std::endl;
		
		}
		double rangeLower = 45;
		double rangeHigher = 115;
		
		
		if(m == 9||true){
			std::cout<<"Combining warm up and cool down with " << nPoints << " interpolated points..."<<std::endl;
			std::cout<<"Channel: " << m <<std::endl;
			TF1* function = new TF1("f1","[0]+[1]*(x-75)+[2]*(x-75)*(x-75)+[3]*(x-75)*(x-75)*(x-75)",rangeLower,rangeHigher);
			grsReduced[m]->Fit("f1","R");
			std::cout<<"[0]+[1]*(R-75)+[2]*(R-75)^2+[3]*(R-75)^3"<<std::endl;
			std::cout<<"_____________________________________________________________________________________________________"<<std::endl;
			if(m!=0 && m!=1)outfile << date<<"\t"<<m<<"\t"<<function->GetParameter(0) << "\t" << function->GetParameter(1) << "\t" << function->GetParameter(2) << "\t" << function->GetParameter(3) <<std::endl;
			delete function;
		}
	}
	
	outfile.close();
}
